#!/bin/bash

if [ -x /usr/bin/xpumcli ]; then
	EXEC=/usr/bin/xpumcli
else
	EXEC=/usr/bin/xpu-smi
fi

for dev in {0..3}; do
	for tile in {0..1}; do
		${EXEC} config --frequencyrange 1100,1100 -d ${dev} -t ${tile}
	done
done
