#!/bin/bash
set -x
/usr/bin/echo "Unloading iaf module"
/sbin/modprobe -r iaf
echo "Printing existing values of iaf_power_enable"
for i in {0..3}; do /usr/bin/cat /sys/class/drm/card$i/iaf_power_enable ; done;
/usr/bin/echo "Setting iaf_power_enable to 0"
for i in {0..3}; do /usr/bin/echo 0 > /sys/class/drm/card$i/iaf_power_enable ; done;
/usr/bin/echo "Printing new values of iaf_power_enable"
for i in {0..3}; do /usr/bin/cat /sys/class/drm/card$i/iaf_power_enable ; done;
