#!/bin/bash

PCSBIN_FILE="$1"
FIRMWARE_FILE="$2"

[ -n "$FIRMWARE_FILE"  -a -n "$PCSBIN_FILE" ] || { echo "Usage: ifwi-update.sh <oam file> <firmware file>" >&2; exit 1; }

set -e
set -x
for device in 0 1 2 3; do
    xpu-smi updatefw -y -d ${device} -t GFX_PSCBIN -f ${PCSBIN_FILE}
    xpu-smi updatefw -y -d ${device} -t GFX -f ${FIRMWARE_FILE}
done
